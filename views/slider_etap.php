<?php

use yii\helpers\Html;
use app\themes\basic\ThemeAssets;

$bundle = ThemeAssets::register($this);
?>
<div class="coItem">
    <?php $item = 1; ?>
    <?php foreach($this->context->objectEtap as $project): ?>
        <?php if($item%4 == 0): ?>
        </div>
        <div class="coItem">
            <div class="col-md-4 oneObject">
                <div class="innerOneObject">
                    <a href="project/<?=$project->alias;?>" class="objectFoto">
                        <img src="<?=$project->profile->urlAttribute('picture');?>"/>
                    </a>
                    <div class="objectPriceBlock">
                        <div class="greyDecoObject">
                            <p>от <span>900 000 руб.</span></p>
                        </div>
                        <div class="redDecoObject">
                            <p>от <span>900 000 руб.</span></p>
                        </div>
                    </div>
                    <p class="objectLink"><a href="project/<?=$project->alias;?>">ЖК «<?=$project->name;?>»</a></p>
                    <div class="dots"></div>
                    <img src="<?=$bundle->baseUrl;?>/images/location.png" class="location"/>
                    <address>г. Краснодар, р-н Гидрострой,<br />ул.Автолюбителей, дом. 1/Д</address>
                    <img src="<?=$bundle->baseUrl;?>/images/iconka1.png"/>
                </div>
            </div>
        <?php else: ?>
        <div class="col-md-4 oneObject">
            <div class="innerOneObject">
                <a href="project/<?=$project->alias;?>" class="objectFoto">
                    <img src="<?=$project->profile->urlAttribute('picture');?>"/>
                </a>
                <div class="objectPriceBlock">
                    <div class="greyDecoObject">
                        <p>от <span>900 000 руб.</span></p>
                    </div>
                    <div class="redDecoObject">
                        <p>от <span>900 000 руб.</span></p>
                    </div>
                </div>
                <p class="objectLink"><a href="project/<?=$project->alias;?>">ЖК «<?=$project->name;?>»</a></p>
                <div class="dots"></div>
                <img src="<?=$bundle->baseUrl;?>/images/location.png" class="location"/>
                <address>г. Краснодар, р-н Гидрострой,<br />ул.Автолюбителей, дом. 1/Д</address>
                <img src="<?=$bundle->baseUrl;?>/images/iconka1.png"/>
            </div>
        </div>
        <?php endif; ?>
        <?php $item++; ?>
    <?php endforeach; ?>
</div>