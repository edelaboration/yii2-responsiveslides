<?php

use yii\helpers\Html;

echo Html::beginTag('ul', ['class' => 'rslides', 'id' => 'slider1']);

    foreach ($this->context->images as $image)
    {
        echo Html::tag('li', $image);
    }

echo Html::endTag('ul');

/*
 * 'maxwidth' => 800,
 * 'auto' => true,
 * 'speed' => 500,
 * */

