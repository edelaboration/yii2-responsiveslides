<?php

use yii\helpers\Html;
use app\themes\basic\ThemeAssets;

$bundle = ThemeAssets::register($this);

foreach ($this->context->projects as $project)
{
    ?>
        <div class="oneSlide">
            <div class="info">
                <a href="/project/<?=$project->alias?>" class="gkLogo">
                    <img src="<?=$bundle->baseUrl?>/images/regata_logo.png"/>
                    <div class="clearfix"></div>
                    <p>Жилой комплекс</p>
                    <h2>«<?=$project->name?>»</h2>
                    <address>г. Краснодар, р-н Гидрострой,<br/> ул.Автолюбителей, дом. 1/Д</address>
                    <div class="priceBlock">
                        <p>от <span>900 000 руб.</span></p>
                    </div>
                </a>
                <a href="#" class="prevGeneralSlide"></a>
                <a href="#" class="nextGeneralSlide"></a>
            </div>
            <div class="slideFoto">
                <div class="slideBorder"></div>
                <img src="<?=$project->profile->urlAttribute('picture')?>"/>
            </div>
        </div>
    <?php
}

//<img src="'.$bundle->baseUrl.'/images/foto1.png"/>