<?php

use yii\helpers\Html;

echo Html::beginTag('ul', ['class' => 'rslides', 'id' => 'slider2']);

    foreach ($this->context->images as $image) {
        echo Html::tag('li', $image);
    }

echo Html::endTag('ul');

/*
* 'auto' => false,
* 'pager' => true,
* 'nav' => true,
* 'speed' => 500,
* 'maxwidth' => 800,
* 'namespace' => 'transparent-btns',
* */