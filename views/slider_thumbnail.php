<?php

use yii\helpers\Html;

/* Images */
echo Html::beginTag('ul', ['class' => 'rslides', 'id' => 'slider3']);

foreach ($this->context->images as $image)
{
    echo Html::tag('li', $image);
}

echo Html::endTag('ul');

/* Pager */
    echo Html::beginTag('ul', ['id' => 'slider3-pager']);

    foreach ($this->context->thumbnails as $thumbnail)
    {
        echo Html::beginTag('li');

        echo Html::tag('a', $thumbnail);

        echo Html::endTag('li');
    }
    echo Html::endTag('ul');

/*
 * 'manualControls' => '#slider3-pager',
 * 'maxwidth' => 540,
 * */

