<?php

namespace mrnicky\slides;

use mrnicky\slides\assets\ResponsiveSlidesAsset;
use yii\base\Widget;
use yii\base\Exception;
use yii\helpers\Json;

use Yii;

class ResponsiveSlides extends Widget
{
    // array options to populate Slick jQuery object
    public $clientOptions = [];

    // array elements for the carousel
    public $images = [];
    
    //myOptions
    public $projects;
    
    //Objects Projected
    public $objectEtap;

    // array elements for the pager
    public $thumbnails = [];

    public $template;
    
    public $container;
    
    public $etap=1;

    public function init()
    {
        $this->projects = \eapanel\catalog\models\Project::find()->roots()->all();
        $this->objectEtap = \eapanel\catalog\models\Project::find()->joinWith('profile')->where("stage=$this->etap")->roots()->all();
    }

    /**
     * Register required scripts for the Slick plugin
     */
    protected function registerClientScript()
    {
        $view = $this->getView();

        ResponsiveSlidesAsset::register($view);

        $options = Json::encode($this->clientOptions);

        $js[] = ";";

        $js[] = "jQuery('.$this->container').responsiveSlides({$options});";

        $view->registerJs(implode(PHP_EOL, $js));
    }

    public function run()
    {
        echo $this->renderFile('@vendor/mrnicky/yii2-responsiveslides/views/slider_'.$this->template.'.php');

        $this->registerClientScript();

    }
}