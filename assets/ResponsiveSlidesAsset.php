<?php

namespace mrnicky\slides\assets;

use yii\web\AssetBundle;

class ResponsiveSlidesAsset extends AssetBundle
{
    public $sourcePath = '@bower/ResponsiveSlides';

    public $css = [
        'responsiveslides.css',
    ];

    public $js = [
        'responsiveslides.min.js'
    ];

    public $depends = [
        'yii\web\JqueryAsset'
    ];
}