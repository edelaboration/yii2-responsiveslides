# ResponsiveSlides widget

The yii 2 widget to create simple [responsive slider.](http://responsiveslides.com/)

### Installation via Composer
Add this line to "require" section of your composer.json file:
```
"mrnicky/yii2-responsiveslides": "dev-master"
```
### Using

```
echo \mrnicky\slides\ResponsiveSlides::widget([
    'template' => 'default',
    'images' => [
        Html::img('/images/1.jpg'),
        Html::img('/images/2.jpg'),
        Html::img('/images/3.jpg'),
    ],
    'clientOptions' => [
        'maxwidth' => 800,
        'auto' => true,
        'speed' => 500,
    ],
]);
```

There are a few templates of view with own clientOptions.

**default**
```
'maxwidth' => 800,
'auto' => true,
'speed' => 500,
```
**dots**
```
'auto' => false,
'pager' => true,
'nav' => true,
'speed' => 500,
'maxwidth' => 800,
'namespace' => 'transparent-btns',
```
**numbers**
```
'auto' => false,
'pager' => true,
'speed' => 300,
'maxwidth' => 540
```
**thumbnail**
```
'manualControls' => '#slider3-pager',
'maxwidth' => 540,
```

For more examples, visit the [Official page of plugin](http://responsiveslides.com/).
